from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = TodoItem.objects.filter(list=todo_list)
    context = {
        "todo_object": todo_list,
        "todo_list_detail": todo_items,
    }

    return render(request, "todos/detail.html", context)
